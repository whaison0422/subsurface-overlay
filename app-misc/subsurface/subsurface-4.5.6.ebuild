# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

MY_P=${P/s/S}
MARBLE_SOVER_MAJOR="0"
MARBLE_SOVER_MINOR="21"
MARBLE_SOVER_REV="3"
MARBLE_SOVER="${MARBLE_SOVER_MAJOR}.${MARBLE_SOVER_MINOR}.${MARBLE_SOVER_REV}"

SRC_URI="https://subsurface-divelog.org/downloads/${MY_P}.tgz
	http://subsurface-divelog.org/downloads/libdivecomputer-subsurface-branch-${PV}.tgz
	http://subsurface-divelog.org/downloads/marble-subsurface-branch-${PV}.tgz"
KEYWORDS="~amd64 ~x86"

PLOCALES="
	bg_BG cs da_DK de_CH de_DE en_GB es_ES et_EE fi_FI fr_FR he it_IT lv_LV
	nb_NO nl_NL pl_PL pt_BR pt_PT ro_RO ru_RU sk_SK sv_SE tr zh_TW
"

inherit eutils l10n autotools cmake-utils

DESCRIPTION="An open source dive log program"
HOMEPAGE="http://subsurface-divelog.org"
LICENSE="GPL-2"
SLOT="0"
IUSE="doc bluetooth facebook" # debug

RDEPEND="
	dev-db/sqlite:3
	dev-libs/grantlee:5
	dev-libs/libusb
	dev-libs/libgit2:=
	dev-libs/libxml2
	dev-libs/libxslt
	dev-libs/libzip
	dev-libs/openssl
	bluetooth? ( dev-qt/qtbluetooth:5 )
	dev-qt/qtconcurrent:5
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtnetwork:5
	dev-qt/qtprintsupport:5
	dev-qt/qtsvg:5
	dev-qt/qtwebkit:5
	dev-qt/qtwidgets:5
	net-libs/libssh2
	net-misc/curl
	sys-libs/zlib
"

DEPEND="${RDEPEND}
	doc? ( app-text/asciidoc )
	dev-qt/linguist-tools:5
	dev-util/cmake
	virtual/pkgconfig
"

DOCS="${MY_P}/README"

S="${WORKDIR}"

src_unpack() {
	unpack ${A}
}

src_prepare() {
	mv libdivecomputer-subsurface-branch* libdc || die "renaming bundled libdivecomputer src failed"
	pushd libdc >/dev/null || die "pushd failed"
	eautoreconf
	popd >/dev/null || die "popd failed"

	mv marble-subsurface-branch* marble || die "renaming bundled marble src failed"

	pushd ${MY_P} >/dev/null || die "pushd failed"
	epatch "${FILESDIR}/${PN}-4.5_grentlee5_include.patch"
	epatch "${FILESDIR}/${PN}-4.5_libgit2-0.24.patch"
	epatch "${FILESDIR}/${PN}-4.5_c++11.patch"
	popd >/dev/null || die "popd failed"
}

src_configure() {
	local mycmakeargs

	einfo "configuring bundled libdivecomputer"
	pushd libdc >/dev/null || die "pushd faild"
	econf
	popd >/dev/null

	einfo "configuring bundled marble"
	BUILD_DIR="${S}/marble-build"
	CMAKE_USE_DIR="${S}/marble"
	mycmakeargs=(
		-DQTONLY=TRUE
		-DQT5BUILD=ON
		-DBUILD_MARBLE_TESTS=NO
		-DWITH_DESIGNER_PLUGIN=NO
		-DBUILD_MARBLE_APPS=NO
	)
	cmake-utils_src_configure

	einfo "configuring subsurface"
	BUILD_DIR="${S}/subsurface-build"
	CMAKE_USE_DIR="${S}/${MY_P}"
	mycmakeargs=(
		-DLIBDIVECOMPUTER_INCLUDE_DIR=${S}/libdc/include/
		-DLIBDIVECOMPUTER_LIBRARIES=${S}/libdc/src/.libs/libdivecomputer.a
		-DMARBLE_INCLUDE_DIR=${S}/marble/src/lib/marble
		-DMARBLE_LIBRARIES=${S}/marble-build/src/lib/marble/libssrfmarblewidget.so
		-DMAKE_TESTS=NO
		$(cmake-utils_use bluetooth BTSUPPORT)
		$(cmake-utils_use facebook FBSUPPORT)
		$(cmake-utils_use_no doc DOCS)
		$(cmake-utils_use_no doc USERMANUAL)
	)
	cmake-utils_src_configure
}

src_compile() {
	einfo "compile bundled libdivecomputer"
	pushd libdc >/dev/null || die "pushd faild"
	emake
	popd >/dev/null

	einfo "compile bundled marble"
	BUILD_DIR="${S}/marble-build"
	CMAKE_USE_DIR="${S}/marble"
	cmake-utils_src_compile

	einfo "compile subsurface"
	BUILD_DIR="${S}/subsurface-build"
	CMAKE_USE_DIR="${S}/${MY_P}"
	cmake-utils_src_compile
}

rm_trans() {
	rm "${ED}/usr/share/${PN}/translations/${PN}_${1}.qm" || die "rm ${PN}_${1}.qm failed"
}

src_install() {
	BUILD_DIR="${S}/subsurface-build"
	CMAKE_USE_DIR="${S}/${MY_P}"
	cmake-utils_src_install

	dolib.so "${S}/marble-build/src/lib/marble/libssrfmarblewidget.so.${MARBLE_SOVER}"
	dosym libssrfmarblewidget.so.${MARBLE_SOVER} "/usr/$(get_libdir)/libssrfmarblewidget.so"
	dosym libssrfmarblewidget.so.${MARBLE_SOVER} "/usr/$(get_libdir)/libssrfmarblewidget.so.${MARBLE_SOVER_MINOR}"

	l10n_for_each_disabled_locale_do rm_trans

	# this is not a translation but present (no need to die if not present)
	rm "${ED}/usr/share/${PN}/translations/${PN}_source.qm"
}
