# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

SSRF_REF="v${PV}"
LIBDC_REF="v${PV}"
MARBLE_REF="v${PV}"
SSRF_SERVER_URI="http://git.subsurface-divelog.org/index.cgi"
MARBLE_SOVER_MAJOR="0"
MARBLE_SOVER_MINOR="21"
MARBLE_SOVER_REV="3"
MARBLE_SOVER="${MARBLE_SOVER_MAJOR}.${MARBLE_SOVER_MINOR}.${MARBLE_SOVER_REV}"

SRC_URI="${SSRF_SERVER_URI}?p=subsurface.git;a=snapshot;h=${SSRF_REF};sf=tgz -> ${P}.tgz
	${SSRF_SERVER_URI}?p=libdc.git;a=snapshot;h=${LIBDC_REF};sf=tgz -> libdivecomputer-subsurface-branch-${PV}.tgz
	${SSRF_SERVER_URI}?p=marble.git;a=snapshot;h=${MARBLE_REF};sf=tgz -> marble-subsurface-branch-${PV}.tgz
"
KEYWORDS="~amd64" # ~x86"

PLOCALES="
	bg_BG cs da_DK de_CH de_DE en_GB es_ES et_EE fi_FI fr_FR he it_IT lv_LV
	nb_NO nl_NL pl_PL pt_BR pt_PT ro_RO ru_RU sk_SK sv_SE tr zh_TW
"

inherit eutils l10n autotools cmake-utils

DESCRIPTION="An open source dive log program"
HOMEPAGE="http://subsurface-divelog.org"
LICENSE="GPL-2"
SLOT="0"
IUSE="doc bluetooth facebook" # debug

RDEPEND="
	dev-db/sqlite:3
	dev-libs/grantlee:5
	dev-libs/libusb
	>=dev-libs/libgit2-0.23:=
	dev-libs/libxml2
	dev-libs/libxslt
	dev-libs/libzip
	dev-libs/openssl
	bluetooth? ( dev-qt/qtbluetooth:5 )
	dev-qt/qtconcurrent:5
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtnetwork:5
	dev-qt/qtprintsupport:5
	dev-qt/qtsvg:5
	dev-qt/qtwebkit:5
	dev-qt/qtwidgets:5
	net-libs/libssh2
	net-misc/curl
	sys-libs/zlib
"

DEPEND="${RDEPEND}
	doc? ( app-text/asciidoc )
	dev-qt/linguist-tools:5
	dev-util/cmake
	virtual/pkgconfig
"

DOCS="subsurface/README"
SSRF_PATCHES=()

S="${WORKDIR}"

src_unpack() {
	unpack ${A}
}

src_prepare() {
	mv subsurface* subsurface || die "renaming subsurface src failed"
	pushd subsurface >/dev/null || die "pushd failed"
	[ ${#SSRF_PATCHES[@]} -gt 0 ] && epatch ${SSRF_PATCHES[@]}
	popd >/dev/null || die "popd failed"

	mv libdc* libdc || die "renaming bundled libdivecomputer src failed"
	pushd libdc >/dev/null || die "pushd failed"
	eautoreconf
	popd >/dev/null || die "popd failed"

	mv marble* marble || die "renaming bundled marble src failed"

	epatch_user
}

src_configure() {
	local mycmakeargs

	einfo "configuring bundled libdivecomputer"
	pushd libdc >/dev/null || die "pushd faild"
	econf
	popd >/dev/null

	einfo "configuring bundled marble"
	BUILD_DIR="${S}/marble-build"
	CMAKE_USE_DIR="${S}/marble"
	mycmakeargs=(
		-DCMAKE_BUILD_TYPE=Release
		-DQTONLY=TRUE
		-DQT5BUILD=ON
		-DBUILD_MARBLE_TESTS=OFF
		-DWITH_DESIGNER_PLUGIN=NO
		-DBUILD_MARBLE_APPS=OFF
		-DBUILD_MARBLE_TOOLS=OFF
		-DBUILD_WITH_DBUS=OFF
		-DBUILD_TESTING=OFF
		-DCMAKE_INSTALL_PREFIX="${BUILD_DIR}"
	)
	cmake-utils_src_configure

	einfo "configuring subsurface"
	BUILD_DIR="${S}/subsurface-build"
	CMAKE_USE_DIR="${S}/${PN}"
	mycmakeargs=(
		-DLIBDIVECOMPUTER_INCLUDE_DIR="${S}/libdc/include"
		-DLIBDIVECOMPUTER_LIBRARIES="${S}/libdc/src/.libs/libdivecomputer.a"
		-DMARBLE_INCLUDE_DIR="${S}/marble-build/include"
		-DMARBLE_LIBRARIES="${S}/marble-build/$(get_libdir)/libssrfmarblewidget.so"
		-DLIBGIT2_FROM_PKGCONFIG=ON
		-DMAKE_TESTS=NO
		-DBTSUPPORT=$(usex bluetooth ON OFF)
		-DFBSUPPORT=$(usex facebook ON OFF)
		-DNO_DOCS=$(usex doc OFF ON)
		-DNO_USERMANUAL=$(usex doc OFF ON)
		-DUSE_LIBGIT23_API=ON
	)
	cmake-utils_src_configure
}

src_compile() {
	einfo "compile bundled libdivecomputer"
	pushd libdc >/dev/null || die "pushd faild"
	emake
	popd >/dev/null

	einfo "compile bundled marble"
	BUILD_DIR="${S}/marble-build/src/lib/marble"
	CMAKE_USE_DIR="${S}/marble"
	cmake-utils_src_compile
	# needed to get all headers and libs at a sane path
	pushd "${BUILD_DIR}" > /dev/null || die "pushd failed"
	${CMAKE_MAKEFILE_GENERATOR} install || die "died running ${CMAKE_MAKEFILE_GENERATOR} install"
	popd > /dev/null || die "popd failed"

	einfo "compile subsurface"
	BUILD_DIR="${S}/subsurface-build"
	CMAKE_USE_DIR="${S}/${PN}"
	cmake-utils_src_compile
}

rm_trans() {
	rm "${ED}/usr/share/${PN}/translations/${PN}_${1}.qm" || die "rm ${PN}_${1}.qm failed"
}

src_install() {
	BUILD_DIR="${S}/subsurface-build"
	CMAKE_USE_DIR="${S}/${PN}"
	cmake-utils_src_install

	dolib.so "${S}/marble-build/$(get_libdir)/libssrfmarblewidget.so.${MARBLE_SOVER}"
	dosym libssrfmarblewidget.so.${MARBLE_SOVER} "/usr/$(get_libdir)/libssrfmarblewidget.so"
	dosym libssrfmarblewidget.so.${MARBLE_SOVER} "/usr/$(get_libdir)/libssrfmarblewidget.so.${MARBLE_SOVER_MINOR}"

	l10n_for_each_disabled_locale_do rm_trans

	# this is not a translation but present (no need to die if not present)
	rm "${ED}/usr/share/${PN}/translations/${PN}_source.qm"
}
